import * as PIXI from 'pixi.js';
import {IResizer, Resizer} from "./utils/resizer";
import {GameController} from "./controller/game-controller";
import {SoundManager} from "./utils/sound-manager";
import { DeviceUtils } from './utils/device-utils';

const HEIGHT = 640;
const WIDTH = 480;

class Game extends PIXI.Container {
    constructor() {
        super();

        new SoundManager();

        const app: PIXI.Application = this.initPixi();

        const resizer:IResizer = new Resizer(app, WIDTH, HEIGHT);
        const controller:GameController = new GameController(app, resizer);

        resizer.onResize((screenSize) => controller.resize(screenSize));

        document.addEventListener("visibilitychange", this.handleVisibilityChange, false);


        const enableAudioContext = () => {
            app.view.removeEventListener('touchend', enableAudioContext);
            SoundManager.resumeAllSounds();
        }

        app.view.addEventListener('touchend', enableAudioContext);
    }

    initPixi(): PIXI.Application {
        const app = new PIXI.Application({
            width: DeviceUtils.isDesktop() ? window.innerWidth * 2 : window.innerWidth * devicePixelRatio,
            height: DeviceUtils.isDesktop() ? window.innerWidth * 2 :  window.innerHeight * devicePixelRatio,
            // @ts-ignore
            autoResize: true,
            backgroundColor: 0x000000,
            resolution: DeviceUtils.isDesktop() ? 2: devicePixelRatio,
            antialias: true,
        });

        document.body.appendChild(app.view);

        return app;
    }

    handleVisibilityChange(): void {
        if (document.hidden) {
            SoundManager.pauseAllSounds();
        } else  {
            SoundManager.resumeAllSounds();
        }
    }
}

export {
    Game,
    WIDTH,
    HEIGHT
};
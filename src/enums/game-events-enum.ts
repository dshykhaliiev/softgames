class GameEventsEnum {
    static get SHOW_MENU_BTN_CLICKED(): string {
        return 'SHOW_MENU_BTN_CLICKED';
    }

    static get MENU_SPRITES_BTN_CLICKED(): string {
        return 'MENU_SPRITES_BTN_CLICKED';
    }

    static get MENU_EMOTICONS_BTN_CLICKED(): string {
        return 'MENU_EMOTICONS_BTN_CLICKED';
    }

    static get MENU_PARTICLES_BTN_CLICKED(): string {
        return 'MENU_PARTICLES_BTN_CLICKED';
    }

}

export { GameEventsEnum };
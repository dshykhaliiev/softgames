import {Tween} from "@tweenjs/tween.js";

const TWEEN = require('@tweenjs/tween.js');

class TimeoutUtils {
    // TODO: replace with focus-losing friendly implementation

    static setTimeout(callback: () => void, delay: number, ctx: any = null): Tween<object> {
        const tween: Tween<object> = new TWEEN.Tween({})
            .to({x: 100}, delay)
            .onComplete(() => {
                callback.call(ctx);
            });

        tween.start();

        return tween;
    }

    static clearTimeout(timeout): void {
        TWEEN.remove(timeout);
    }

    static setInterval(callback: () => void, delay: number): Tween<object> {
        const obj = {x: 0};

        const tween = new TWEEN.Tween(obj)
            .to({x: 100}, delay)
            .repeat(Infinity)
            .onUpdate(() => {
                if (obj.x === 100) {
                    callback();
                }
            });

        tween.start();

        return tween;
    }

    static clearInterval(interval): void {
        TWEEN.remove(interval);
    }

    static setLimitedInterval(
        callback: () => void,
        delay: number,
        repeat: number,
        onCompleteCb = () => {}
    ) {
        let amountLeft: number = repeat;
        const interval = TimeoutUtils.setInterval(() => {
            callback();

            amountLeft--;
            if (amountLeft <= 0) {
                TimeoutUtils.clearInterval(interval);
                onCompleteCb();
            }
        }, delay);
    }
}

export { TimeoutUtils }
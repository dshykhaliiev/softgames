export interface ISound {
    source: AudioBufferSourceNode;
    gainNode: GainNode;
}

class SoundManager {
    constructor() {
        const AudioContext: any = window.AudioContext || window['webkitAudioContext'];
        const audioCtx: AudioContext = new AudioContext();

        SoundManager['audioCtx'] = audioCtx;
    }

    static playSound(sound: string, isLoop: boolean = false, volume: number = 1): ISound {
        const audioCtx = SoundManager['audioCtx'];
        let gainNode = audioCtx.createGain();
        SoundManager['gainNode'] = gainNode;
        gainNode.connect(audioCtx.destination);
        gainNode.gain.value = volume;

        let source;
        source = audioCtx.createBufferSource();
        source.buffer = SoundManager['_assets'].audio[sound];
        source.loop = isLoop;
        source.start();
        source.connect(gainNode);

        return {gainNode, source};
    }

    static set assets(assets: Record<string, PIXI.LoaderResource>) {
        SoundManager['_assets'] = assets;
    }

    static pauseAllSounds() {
        SoundManager['audioCtx'].suspend();
    }

    static resumeAllSounds() {
        SoundManager['audioCtx'].resume();
    }
}

export {
    SoundManager
}
'use strict';

import {DeviceUtils} from "./device-utils";
import * as PIXI from 'pixi.js';
import { TimeoutUtils } from './timeout-utils';
import {HEIGHT, WIDTH} from "../game";

export interface IScreenSize {
    width: number;
    height: number;
    isPortrait: boolean;
}

export interface IResizer {
    width: number;
    height: number;
    onResize: (fn: (screenSize: IScreenSize) => void) => void;
    isPortrait: boolean;
    scale: number;
    screenSize: IScreenSize;
}

class Resizer implements IResizer {
    _width: number;
    _height: number;
    _gameWidth: number;
    _gameHeight: number;
    _isPortrait: boolean;
    _scale: number;
    debugEnabled: boolean = false;
    callbacks: Record<string, (screenSize: IScreenSize) => void>;
    app: PIXI.Application;
    iPad: PIXI.Graphics;
    iPhone: PIXI.Graphics;

    constructor(app: PIXI.Application, gameWidth: number, gameHeight: number) {
        this._width = gameWidth;
        this._height = gameHeight;

        this._gameWidth = gameWidth;
        this._gameHeight = gameHeight;

        this._isPortrait = true;
        this._scale = 1;

        this.callbacks = {};

        console.log('init resizer');

        this.app = app;

        // this.debugEnabled = true;

        window.addEventListener('resize', () => {
            this.fitRendererToWindow();
        });

        this.fitRendererToWindow();
    }

    createDebug(): void {
        // TODO: For debug purpose only
        this.createIPhoneDebug();
        this.createIPadDebug();
    }

    createIPadDebug(): void {
        if (this.iPad) {
            this.app.stage.removeChild(this.iPad);
            this.iPad.destroy();
        }

        const squareSize = 10;
        const scrW = this.isPortrait ? 768 : 1024;
        const scrH = this.isPortrait ? 1024 : 768;


        const iPad = new PIXI.Graphics();
        iPad.beginFill(0x0000FF, 0.3);
        iPad.drawRect(-scrW / 2, -scrH / 2, scrW, scrH);

        iPad.beginFill(0x00FF00, 1);
        iPad.drawRect(-scrW / 2, -scrH / 2, 10, 10);
        iPad.drawRect(scrW / 2 - squareSize, -scrH / 2, 10, 10);
        iPad.drawRect(scrW / 2 - squareSize, scrH / 2 - squareSize, 10, 10);
        iPad.drawRect(-scrW / 2, scrH / 2 - squareSize, 10, 10);
        iPad.endFill();

        if (this.isPortrait) {
            iPad.height = this.height;
            iPad.scale.x = iPad.scale.y;
        }
        else {
            iPad.width = this.width;
            iPad.scale.y = iPad.scale.x;
        }

        this.iPad = iPad;

        TimeoutUtils.setInterval(this.updateIPadDebug.bind(this), 1000);
    }

    createIPhoneDebug(): void {
        if (this.iPhone) {
            this.app.stage.removeChild(this.iPhone);
            this.iPhone.destroy();
        }

        const squareSize = 10;
        const scrW = this.isPortrait ? 375 : 812;
        const scrH = this.isPortrait ? 812: 375;

        const iPhone = new PIXI.Graphics();
        iPhone.beginFill(0x00FFF2, 0.35);
        iPhone.drawRect(-scrW / 2, -scrH / 2, scrW, scrH);

        iPhone.beginFill(0xFF0000, 1);
        iPhone.drawRect(-scrW / 2, -scrH / 2, 10, 10);
        iPhone.drawRect(scrW / 2 - squareSize, -scrH / 2, 10, 10);
        iPhone.drawRect(scrW / 2 - squareSize, scrH / 2 - squareSize, 10, 10);
        iPhone.drawRect(-scrW / 2, scrH / 2 - squareSize, 10, 10);
        iPhone.endFill();

        if (this.isPortrait) {
            iPhone.height = this.height;
            iPhone.scale.x = iPhone.scale.y;
        }
        else {
            iPhone.width = this.width;
            iPhone.scale.y = iPhone.scale.x;
        }

        this.iPhone = iPhone;

        TimeoutUtils.setInterval(this.updateIPhoneDebug.bind(this), 1000);
    }

    updateIPhoneDebug(): void {
        this.app.stage.addChild(this.iPhone);
    }

    updateIPadDebug(): void {
        this.app.stage.addChild(this.iPad);
    }

    onResize(callback): void {
        this.callbacks[callback] = callback;
    }

    get width(): number {
        return this._width / this._scale;
    }

    get height(): number {
        return this._height / this._scale;
    }

    get isPortrait(): boolean {
        return this._isPortrait;
    }

    get scale(): number {
        return this._scale;
    }

    get screenSize(): IScreenSize {
        return {
            width: this.width,
            height: this.height,
            isPortrait: this.isPortrait
        }
    }

    fitRendererToWindow(): void {
        const getInnerWidth = (): number => {
            return window.innerWidth;
        }

        const getInnerHeight = (): number => {
            return window.innerHeight;
        }

        this._width = getInnerWidth();
        this._height = getInnerHeight();

        // landscape
        if (!DeviceUtils.isDesktop() && window.innerWidth > window.innerHeight) {
            this._isPortrait = false;

            const scale = (window.innerWidth / this._gameWidth);
            this.app.stage.scale.set(scale, scale);
            this._scale = scale;
        }
        else { // portrait
            this._isPortrait = true;
            const scale = (getInnerHeight() / this._gameHeight);
            this.app.stage.scale.set(scale, scale);
            this._scale = scale;
        }

        this.app.stage.x = getInnerWidth() * .5;
        this.app.stage.y = getInnerHeight() * .5;

        this.app.renderer.resize(getInnerWidth(), getInnerHeight());

        for (let key in this.callbacks) {
            const callback = this.callbacks[key];
            callback(this.screenSize);
        }

        if (this.debugEnabled) {
            this.createDebug();
        }
    }
}

export {
    Resizer,
};
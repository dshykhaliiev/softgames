export interface IEventBus {
    on: (name: string, callback: (...args) => void, ctx?: any) => IEventBus;
    once: (name: string, callback: (...args) => void, ctx?: any) => IEventBus;

    off: (name: string) => IEventBus;
    emit: (name: string, ...params) => IEventBus;
}

type TCallback = {
    callback: (...args) => void;
    once: boolean;
    ctx: any
}

class EventEmitter implements IEventBus {
    pool: Record<string, TCallback[]>

    constructor() {
        this.pool = {};
    }

    on(name: string, callback: () => void, ctx: any = null): IEventBus {
        this.ensure(name);

        this.pool[name].push({ callback, once: false, ctx: ctx });
        return this;
    }

    once(name: string, callback: (...args) => void, ctx: any): IEventBus {
        this.ensure(name);

        this.pool[name].push({ callback, once: true, ctx: ctx });

        return this;
    }

    off(name: string): IEventBus {
        if (this.has(name)) {
            delete this.pool[name];
        }

        return this;
    }

    emit(name: string, ...params): IEventBus {
        if (this.has(name)) {
            this.pool[name].forEach(({ callback, once, ctx }, index) => {
                if (ctx) {
                    callback.call(ctx, name, ...params)
                }
                else {
                    callback(name, ...params);
                }

                if (once) {
                    this.pool[name].splice(index, 1);
                }
            });
        }

        return this;
    }

    has(name: string): boolean {
        return typeof this.pool[name] !== 'undefined';
    }

    ensure(name: string): void {
        if (!this.has(name)) {
            this.pool[name] = [];
        }
    }
}

export const eventBus: IEventBus = new EventEmitter();
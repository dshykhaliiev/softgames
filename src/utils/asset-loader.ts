import * as PIXI from "pixi.js";

class AssetLoader {
    assets: {[key: string]: any};
    context: any;

    constructor() {
        this.assets = {};

        // @ts-ignore
        this.context = require.context('../../assets', true, /\.(png|jpe?g|svg|mp4|webm|mp3|wav|xml|webp)$/);
    }

    load(): Promise<null> {
        return new Promise(this.loadImageAssets.bind(this))
            .then(this.loadAudioAssets.bind(this))
    }

    loadImageAssets(resolve: () => void): void {
        const images: string[] = this.context.keys().filter((asset): string[] => {
            return asset.includes(`images/`);
        });

        const loader = new PIXI.Loader();

        images.forEach((image: string): void => {
            const index: number = image.lastIndexOf('/');
            const fileName: string = image.substring(index + 1);

            const dotIndex: number = fileName.indexOf('.');
            const name: string = fileName.substring(0, dotIndex);

            loader.add(name, require(`../../assets/images/${fileName}`));
        });

        loader.load((loader: PIXI.Loader, resources:Partial<Record<string, PIXI.LoaderResource>>) => {
            this.assets.images = resources;
            resolve();
        });
    }

    loadAudioAssets(): Promise<{[key: string]: any}> {
        return new Promise((resolve) => {
            const audioData = {};

            const load = require('audio-loader');
            const audio: string[] = this.context.keys().filter((asset): string[] => {
                return asset.includes(`audio/`);
            });

            audio.forEach((audio: string) => {
                const index: number = audio.lastIndexOf('/');
                const fileName: string = audio.substring(index + 1);

                const dotIndex: number = fileName.indexOf('.');
                const name: string = fileName.substring(0, dotIndex);

                audioData[name] = require(`../../assets/audio/${fileName}`);
            });

            load(audioData,)
                .then((dataLoaded: Partial<Record<string, PIXI.LoaderResource>>) => {
                    this.assets.audio = dataLoaded;
                    resolve(this.assets);
                });
        });
    }
}

export {AssetLoader};
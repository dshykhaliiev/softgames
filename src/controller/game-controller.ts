import {eventBus} from "../utils/events";
import {GameEventsEnum} from "../enums/game-events-enum";
import {AssetLoader} from "../utils/asset-loader";
import {SoundManager} from "../utils/sound-manager";
const TWEEN = require('@tweenjs/tween.js');
import * as PIXI from 'pixi.js';
import {IResizer, IScreenSize} from "../utils/resizer";
import {MenuView} from "../view/menu-view";
import {IBaseView} from "../view/base-view";
import {SpritesView} from "../view/sprites-view";
import {ParticlesView} from "../view/particles-view";
import {EmoticonsView} from "../view/emoticons-view";
import {Fps} from "../view/components/fps";

class GameController {
    app: PIXI.Application;
    resizer: IResizer;
    assets: Record<string, PIXI.LoaderResource>;
    fpsLayer: PIXI.Container;
    fps: Fps;
    viewsLayer: PIXI.Container;
    currentView: IBaseView;

    constructor(app: PIXI.Application, resizer: IResizer) {
        this.app = app;
        this.resizer = resizer;

        app.ticker.add(this.update, this);

        this.init();

        this.addListeners();
    }


    init(): void {
        const assetLoader: AssetLoader = new AssetLoader();
        assetLoader
            .load()
            .then(this.saveAssets.bind(this))
            .then(this.startGame.bind(this));
    }

    saveAssets(assets: Record<string, PIXI.LoaderResource>): void {
        SoundManager.assets = assets;

        this.assets = assets;
    }

    startGame(): void {
        this.viewsLayer = new PIXI.Container();
        this.app.stage.addChild(this.viewsLayer);

        this.fpsLayer = new PIXI.Container();
        this.app.stage.addChild(this.fpsLayer);

        this.showFps();

        this.showMenu();
    }

    showFps():void {
        this.fps = new Fps(this.resizer);
        this.fpsLayer.addChild(this.fps);
    }

    showMenu(): void {
        this.hideCurrentView();

        this.currentView = new MenuView(this.assets, this.resizer);
        this.currentView.resize();
        this.viewsLayer.addChild(this.currentView);
    }

    showSpritesView() {
        this.hideCurrentView();

        this.currentView = new SpritesView(this.assets, this.resizer);
        this.currentView.resize();
        this.viewsLayer.addChild(this.currentView);
    }

    showParticlesView() {
        this.hideCurrentView();

        this.currentView = new ParticlesView(this.assets, this.resizer);
        this.currentView.resize();
        this.viewsLayer.addChild(this.currentView);
    }

    showEmoticonsView() {
        this.hideCurrentView();

        this.currentView = new EmoticonsView(this.assets, this.resizer);
        this.currentView.resize();
        this.viewsLayer.addChild(this.currentView);
    }

    addListeners() {
        eventBus.on(GameEventsEnum.SHOW_MENU_BTN_CLICKED, this.showMenu, this);
        eventBus.on(GameEventsEnum.MENU_EMOTICONS_BTN_CLICKED, this.showEmoticonsView, this);
        eventBus.on(GameEventsEnum.MENU_SPRITES_BTN_CLICKED, this.showSpritesView, this);
        eventBus.on(GameEventsEnum.MENU_PARTICLES_BTN_CLICKED, this.showParticlesView, this);
    }

    hideCurrentView(): void {
        if (this.currentView) {
            this.viewsLayer.removeChild(this.currentView);
            this.currentView.dispose();
            this.currentView = null;
        }
    }

    update(delta: number): void {
        if (this.fps) {
            this.fps.update();
        }

        if (this.currentView) {
            this.currentView.update(delta);
        }

        TWEEN.update();
    }

    resize(screenSize: IScreenSize): void {
        if (this.currentView) {
            this.currentView.resize(screenSize);
        }

        if (this.fps) {
            this.fps.resize();
        }
    }
}

export {GameController};
import * as PIXI from 'pixi.js';
import { IResizer } from '../utils/resizer';
import {BaseView} from "./base-view";
import { Tween } from '@tweenjs/tween.js';
import { TimeoutUtils } from '../utils/timeout-utils';
const TWEEN = require('@tweenjs/tween.js');

const CARDS_NUM: number = 144;
const LEFT_STACK_X: number = -80;
const RIGHT_STACK_X: number = 80;
const START_Y_POS: number = 250;
const CARD_HEIGHT: number = 3;
const FLY_DURATION: number = 2000;
const FLY_INTERVAL: number = 1000;

class SpritesView extends BaseView {
    container: PIXI.Container;
    cards: PIXI.Sprite[];
    timeouts: Tween<object>[];

    constructor(assets: Record<string, PIXI.LoaderResource>, resizer: IResizer) {
        super(assets, resizer);

        this.cards = [];
        this.timeouts = [];

        this.container = new PIXI.Container();
        this.addChild(this.container);

        this.createCards();

        const startTimeout = TimeoutUtils.setTimeout(() => {
            this.startAnimation();
        }, 500);
        this.timeouts.push(startTimeout);
    }

    createCards(): void {
        for (let i = 0; i < CARDS_NUM; i++) {
            const card = new PIXI.Sprite(this.assets.images['card_back'].texture);
            card.anchor.set(0.5);
            card.scale.set(0.15);
            card.position.x = LEFT_STACK_X;
            card.position.y = START_Y_POS - i * CARD_HEIGHT;
            this.container.addChild(card);

            this.cards.push(card);
        }
    }

    startAnimation(): void {
        this.cards.forEach((card : PIXI.Sprite, index: number) => {
            new Tween(card)
                .to({x: RIGHT_STACK_X}, FLY_DURATION)
                .delay((this.cards.length - index) * FLY_INTERVAL)
                .easing(TWEEN.Easing.Linear.None)
                .start();

            const newY  = START_Y_POS - (this.cards.length - index) * CARD_HEIGHT;
            new Tween(card)
                .to({y: newY}, FLY_DURATION)
                .delay((this.cards.length - index) * FLY_INTERVAL)
                .easing(TWEEN.Easing.Sinusoidal.In)
                .start();

            const changeLayerTimeout: Tween<object> = TimeoutUtils.setTimeout(() => {
                this.container.removeChild(card);
                this.container.addChild(card);
            }, (this.cards.length - index) * FLY_INTERVAL)
            this.timeouts.push(changeLayerTimeout);
        })
    }

    resize():void {
        super.resize();

        if (this.resizer.isPortrait) {
            this.container.scale.set(1);
        }
        else {
            this.container.scale.set(0.4);
        }
    }

    dispose() {
        this.timeouts.forEach((timeout: Tween<object>) => {
            TimeoutUtils.clearTimeout(timeout);
        });

        TWEEN.removeAll();
    }
}

export {
    SpritesView
};
import {IResizer, IScreenSize} from "../utils/resizer";
import * as PIXI from 'pixi.js';
import {Button} from "./components/button";
import {eventBus} from "../utils/events";
import {GameEventsEnum} from "../enums/game-events-enum";

export interface IBaseView extends PIXI.Container {
    resize: (screenSize?: IScreenSize) => void;
    dispose: () => void;
    update: (delta: number) => void;
}

class BaseView extends PIXI.Container implements IBaseView {
    bg: PIXI.Sprite;
    resizer: IResizer;
    assets: Record<string, PIXI.LoaderResource>;
    menuBtn: Button;

    constructor(assets: Record<string, PIXI.LoaderResource>, resizer: IResizer) {
        super();

        this.assets =  assets;
        this.resizer = resizer;

        this.width = resizer.width;
        this.height = resizer.height;

        this.createBg();

        this.createMenuBtn();
    }

    createBg(): void {
        this.bg = new PIXI.Sprite(this.assets.images['bg'].texture);
        this.bg.anchor.set(0.5, 0.5);
        this.bg.width = this.resizer.width;
        this.bg.height = this.resizer.height;
        this.addChild(this.bg);
    }

    createMenuBtn(): void {
        this.menuBtn = new Button('', this.assets, true);
        this.menuBtn.on('pointerdown', this.onMenuBtnClick);
        this.addChild(this.menuBtn);
    }

    onMenuBtnClick(): void {
        eventBus.emit(GameEventsEnum.SHOW_MENU_BTN_CLICKED);
    }

    resize(screenSize: IScreenSize = null): void {
        if (this.menuBtn) {
            this.menuBtn.x = this.resizer.width * 0.5 - this.menuBtn.width * 0.8;
            this.menuBtn.y = -this.resizer.height * 0.5 + this.menuBtn.height * 0.8;
        }

        if (this.bg) {
            this.bg.width = this.resizer.width;
            this.bg.height = this.resizer.height;
        }
    }

    dispose(): void {
        if (this.menuBtn) {
            this.menuBtn.off('pointerdown', this.onMenuBtnClick);
            this.menuBtn.dispose();
            this.menuBtn = null;
        }
    }

    update(delta: number): void {

    }
}

export {
    BaseView,
}
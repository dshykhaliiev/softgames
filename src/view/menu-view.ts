import * as PIXI from 'pixi.js';
import {eventBus} from "../utils/events";
import {GameEventsEnum} from "../enums/game-events-enum";
import {Button} from "./components/button";
import {IResizer, IScreenSize} from "../utils/resizer"
import {BaseView, IBaseView} from "./base-view";

const BTNS_GAP = 20;

class MenuView extends BaseView implements IBaseView {
    btnsContainer: PIXI.Container;
    spritesBtn: Button;
    emoticonsBtn: Button;
    particlesBtn: Button;


    constructor(assets: Record<string, PIXI.LoaderResource>, resizer: IResizer) {
        super(assets, resizer);

        this.createButtons();
    }

    createMenuBtn() {
        // don't create menu button
    }

    createButtons() {
        this.btnsContainer = new PIXI.Container();
        this.addChild(this.btnsContainer);

        let buttonIndex = 0;
        let button = new Button('Sprites', this.assets);
        button.on('pointerdown', this.onSpritesBtnClicked);
        button.position.set(0, buttonIndex * (button.height + BTNS_GAP) + button.height * 0.5);
        this.btnsContainer.addChild(button);
        this.spritesBtn = button;

        buttonIndex++;
        button = new Button('Emoticons', this.assets);
        button.on('pointerdown', this.onEmoticonsBtnClicked);
        button.position.set(0, buttonIndex * (button.height + BTNS_GAP) + button.height * 0.5);
        this.btnsContainer.addChild(button);
        this.emoticonsBtn = button;

        buttonIndex++;
        button = new Button('Particles', this.assets);
        button.on('pointerdown', this.onParticlesBtnClicked);
        button.position.set(0, buttonIndex * (button.height + BTNS_GAP) + button.height * 0.5);
        this.btnsContainer.addChild(button);
        this.particlesBtn = button;
    }

    onSpritesBtnClicked(): void {
        eventBus.emit(GameEventsEnum.MENU_SPRITES_BTN_CLICKED);
    }

    onEmoticonsBtnClicked(): void {
        eventBus.emit(GameEventsEnum.MENU_EMOTICONS_BTN_CLICKED);
    }

    onParticlesBtnClicked(): void {
        eventBus.emit(GameEventsEnum.MENU_PARTICLES_BTN_CLICKED);
    }

    resize(screenSize: IScreenSize = null): void {
        super.resize(screenSize);

        this.btnsContainer.position.set(0, -this.btnsContainer.height * 0.5);
    }

    dispose() {
        if (this.spritesBtn) {
            this.spritesBtn.off('pointerdown', this.onSpritesBtnClicked);
            this.spritesBtn.dispose();
            this.spritesBtn = null;
        }

        if (this.emoticonsBtn) {
            this.emoticonsBtn.off('pointerdown', this.onSpritesBtnClicked);
            this.emoticonsBtn.dispose();
            this.emoticonsBtn = null;
        }

        if (this.particlesBtn) {
            this.particlesBtn.off('pointerdown', this.onSpritesBtnClicked);
            this.particlesBtn.dispose();
            this.particlesBtn = null;
        }
    }
}

export {
    MenuView
};
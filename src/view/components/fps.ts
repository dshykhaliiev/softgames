import * as PIXI from 'pixi.js';
import {IResizer, IScreenSize} from "../../utils/resizer";

class Fps extends PIXI.Container {
    tf: PIXI.Text;
    resizer: IResizer;

    constructor(resizer: IResizer) {
        super();

        this.resizer = resizer;

        this.createText();

        this.resize();
    }

    createText():void {
        const style = {
            fontSize: 20,
            align: 'center',
            fill: '#ffffff',
        };

        const text = new PIXI.Text('', style);
        text.anchor.set(0);
        text.resolution = 2;
        this.addChild(text);

        this.tf = text;
    }

    update(): void {
        this.tf.text = `FPS: ${Math.round(PIXI.Ticker.shared.FPS)}`;
    }

    resize(): void {
        this.tf.x = -this.resizer.width * 0.5 + 10;
        this.tf.y = -this.resizer.height * 0.5 + 10;
    }
}

export {
    Fps,
}
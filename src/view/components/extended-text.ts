import * as PIXI from 'pixi.js';

class ExtendedText extends PIXI.Container {
	container: PIXI.Container;
	label: string;
	assets: Record<string, PIXI.LoaderResource>;
	lastPart: PIXI.Text | PIXI.Sprite;
	size: number;

	constructor(assets: Record<string, PIXI.LoaderResource>, size: number) {
		super();

		this.assets = assets;
		this.size = size;

		this.init();
	}

	init(): void {
		this.container = new PIXI.Container();
		this.addChild(this.container);
	}

	set text(label: string) {
		this.label = label;
		this.parseText(label);
	}

	parseText(label: string): void {
		this.parseNextPart(label);

		this.container.x = -this.container.width / 2;
	}

	parseNextPart(label: string): void {
		let imgIndex = label.indexOf('${');

		const style = {
			fontSize: this.size,
			align: 'center',
			fill: '#f3d8a8',
		};

		if (imgIndex >= 0) {
			const textPart = label.slice(0, imgIndex);
			const tf = new PIXI.Text(textPart, style);
			tf.position.x = this.lastPart ? this.lastPart.x + this.lastPart.width: 0;
			this.container.addChild(tf);

			label = label.replace(textPart, '');
			const closeIndex = label.indexOf('}');
			const tag = label.slice(0, closeIndex + 1);

			const imgName = tag.slice(2, tag.length - 1);
			const sprite = new PIXI.Sprite(this.assets.images[imgName].texture);
			sprite.anchor.set(0);
			sprite.height = tf.height || this.size;
			sprite.width = tf.height;
			sprite.position.x = tf.x + tf.width;
			this.container.addChild(sprite);

			this.lastPart = sprite;

			label = label.replace(tag, '');

			if (label.indexOf('${') >= 0) {
				this.parseNextPart(label);
			}
			else {
				this.addLastPart(label, style);
				return;
			}
		}
		else {
			this.addLastPart(label, style);
		}
	}

	addLastPart(label: string, style: Object): void {
		const offset = this.lastPart ? this.lastPart.x + this.lastPart.width : 0;
		const tf = new PIXI.Text(label, style);
		tf.x = offset;
		this.container.addChild(tf);
	}
}

export {
	ExtendedText,
}
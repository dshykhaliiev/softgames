import * as PIXI from 'pixi.js';
import {SoundManager} from "../../utils/sound-manager";

class Button extends PIXI.Container {
    assets: Record<string, PIXI.LoaderResource>;
    label: string;
    isMenu: boolean;
    
    constructor(label: string, assets: Record<string, PIXI.LoaderResource>, isMenu:boolean = false) {
        super();

        this.assets = assets;
        this.label = label;
        this.isMenu = isMenu;

        this.createBg();

        this.createLabel();

        this.interactive = true;

        this.on('pointerdown', this.onClick, this);
    }

    createBg(): void {
        const iconName = this.isMenu ? 'menu_btn' : 'button';

        const sprite = new PIXI.Sprite(this.assets.images[iconName].texture);
        sprite.scale.set(0.3);
        sprite.anchor.set(0.5);

        this.addChild(sprite);
    }

    createLabel(): void {
        const style = {
            fontSize: 20,
            align: 'center',
            fill: '#ffffff',
        };

        const text: PIXI.Text = new PIXI.Text(this.label, style);
        text.anchor.set(0.5);
        text.resolution = 2;
        this.addChild(text);
    }

    onClick(): void {
        SoundManager.playSound('click');
    }

    dispose() {
        this.off('pointerdown', this.onClick);
    }
}

export {
    Button,
};
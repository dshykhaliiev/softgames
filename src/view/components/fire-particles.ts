import * as PIXI from 'pixi.js';
import Proton from 'proton-engine';
import { IResizer } from '../../utils/resizer';

const MAX_PARTICLES: number = 10;
const DEFAULT_RATE: Proton.Rate = new Proton.Rate(
    new Proton.Span(0.16, 0.18),
    new Proton.Span(0.155, 0.12)
);
const STOPPED_RATE: Proton.Rate = new Proton.Rate(
    new Proton.Span(0, 0),
    new Proton.Span(0, 0)
);

class FireParticles extends PIXI.Container {
    container: PIXI.Container;
    assets: Record<string, PIXI.LoaderResource>;
    resizer: IResizer;
    proton: Proton;
    emitter: Proton.Emitter;
    isLimitReached: boolean;

    constructor(assets: Record<string, PIXI.LoaderResource>, resizer: IResizer) {
        super();

        this.assets = assets;
        this.resizer = resizer;

        this.isLimitReached = false;

        this.init();
    }

    init(): void {
        this.createStaticSprite();

        this.createProton();

        this.createEmitter();
    }

    createStaticSprite(): void {
        const sprite = new PIXI.Sprite(this.assets.images['fire_particle'].texture);
        sprite.anchor.set(0.5);
        sprite.scale.set(1.8);
        sprite.blendMode = PIXI.BLEND_MODES.ADD;
        sprite.tint = 0xD86C00;
        sprite.alpha = 0.5;
        this.addChild(sprite);
    }

    createProton(): void {
        this.container = new PIXI.Container();
        this.addChild(this.container);

        this.proton = new Proton();

        const renderer = new Proton.PixiRenderer(this.container);
        renderer.setPIXI(PIXI);
        renderer.color = true;
        renderer.blendMode = PIXI.BLEND_MODES.ADD;
        this.proton.addRenderer(renderer);
    }

    createEmitter(): void {
        const img: PIXI.Texture = this.assets.images['fire_particle'].texture;

        const emitter: Proton.Emitter = new Proton.Emitter();
        emitter.rate = DEFAULT_RATE;

        emitter.addInitialize(new Proton.Body(img));
        emitter.addInitialize(new Proton.Mass(1));
        emitter.addInitialize(new Proton.Life(0.5, 1));
        emitter.addInitialize(new Proton.Velocity(
            new Proton.Span(-0.1, 0.1),
            new Proton.Span(-1.6, -1.8, false),
        ));

        emitter.addBehaviour(new Proton.Alpha(0.82, 0));
        emitter.addBehaviour(new Proton.Scale(
            [2.2, 2.4],
            [0.4, 0.4]
        ));
        emitter.addBehaviour(new Proton.Color("#d84c00", "#fbc531"));
        emitter.addBehaviour(new Proton.RandomDrift(0.8, 0.8, 5));

        this.proton.addEmitter(emitter);

        emitter.emit();

        this.emitter = emitter;
    }

    update(): void {
        if (this.emitter.particles.length >= MAX_PARTICLES) {
            this.emitter.rate = STOPPED_RATE;
            this.isLimitReached = true;
        }
        else {
            if (this.isLimitReached) {
                this.emitter.rate = DEFAULT_RATE;
            }

            this.isLimitReached = false;
        }

        this.emitter.p.x = Math.random() * 5;

        this.proton.update();
    }

    dispose(): void {
        if (this.emitter) {
            this.emitter.stop();
            this.emitter.destroy();
            this.emitter = null;
        }

        if (this.proton) {
            this.proton.destroy();
            this.proton = null;
        }
    }
}

export {
    FireParticles,
}
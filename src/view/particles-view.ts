import * as PIXI from 'pixi.js';
import {IResizer} from "../utils/resizer"
import {BaseView} from "./base-view";
import {FireParticles} from "./components/fire-particles";

class ParticlesView extends BaseView {
    particles: FireParticles;

    constructor(assets: Record<string, PIXI.LoaderResource>, resizer: IResizer) {
        super(assets, resizer);

        this.createParticles();
    }

    createParticles(): void {
        this.particles = new FireParticles(this.assets, this.resizer);
        this.particles.y = 50;
        this.addChild(this.particles);
    }

    update(delta: number): void {
        if (this.particles) {
            this.particles.update();
        }
    }

    dispose(): void {
        if (this.particles) {
            this.particles.dispose();
            this.particles = null;
        }

        super.dispose();
    }
}

export {
    ParticlesView
};
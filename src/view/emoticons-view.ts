import * as PIXI from 'pixi.js';
const TWEEN = require('@tweenjs/tween.js');
import { IResizer } from '../utils/resizer';
import { BaseView } from './base-view';
import { ExtendedText } from './components/extended-text';
import { utils } from '../utils/utils';
import {TimeoutUtils} from "../utils/timeout-utils";
import { Easing, Tween } from '@tweenjs/tween.js';

const PARTS: string[] = [
    '${hungry}', '${spa}', '${scared}', '${santa}', '${thinking}',
    '${smile_octopus}', '${smile_chicken}',
    'Hello! ', 'Nice! ', 'Well done! ', 'Hi! ', 'Good job! ', 'Guten Tag! ',
    'Guten Morgen! ', 'Toll! ', 'Prima! ', 'Wunderbar! ', 'Wonderful! ', 'Marvelous! ',
    'Brilliant! ',
];

const SHOW_NEXT_TIMEOUT: number = 2000;
const HIDE_TIMEOUT: number = 2000;
const SHOW_DURATION: number = 800;
const HIDE_DURATION: number = 550;

class EmoticonsView extends BaseView {
    texts: ExtendedText[];
    showNextInterval: any;
    timeouts: Tween<object>[];

    constructor(assets: Record<string, PIXI.LoaderResource>, resizer: IResizer) {
        super(assets, resizer);

        this.texts = [];
        this.timeouts = [];

        this.createText();

        this.showNextInterval = TimeoutUtils.setInterval((): void => {
            this.createText();
        }, SHOW_NEXT_TIMEOUT);
    }

    createText() {
        let resultStr = '';

        const partNum = utils.getRandomInt(1, 6);
        for (let i = 0; i < partNum; i++) {
            const partIdx = utils.getRandomInt(0, PARTS.length - 1);
            const part = PARTS[partIdx];

            resultStr += part;
        }

        const size = utils.getRandomInt(10, 40);
        const text = new ExtendedText(this.assets, size);
        text.text = resultStr;
        text.position.set(
            utils.getRandomInt(-this.resizer.width * 0.3, this.resizer.width * 0.3),
            utils.getRandomInt(-this.resizer.height * 0.3, this.resizer.height * 0.3)
        );

        text.alpha = 0;
        this.addChild(text);

        new TWEEN.Tween(text)
            .to({alpha: 1}, SHOW_DURATION)
            .easing(Easing.Quadratic.In)
            .start();

        const timeout = TimeoutUtils.setTimeout(() => {
            new TWEEN.Tween(text)
                .to({alpha: 0}, HIDE_DURATION)
                .easing(Easing.Quadratic.In)
                .onComplete(() => {
                    this.removeChild(text);
                    const idx = this.texts.indexOf(text);
                    this.texts.splice(idx, 1);
                })
                .start();
        }, HIDE_TIMEOUT);

        this.timeouts.push(timeout);
    }

    dispose() {
        TWEEN.removeAll();

        this.timeouts.forEach((timeout) => {
            TimeoutUtils.clearTimeout(timeout);
        })

        super.dispose();
    }
}

export {
    EmoticonsView
};
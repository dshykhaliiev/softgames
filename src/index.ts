import { Game } from "./game";

declare global {
    interface Window {
        removeLoader: () => void;
    }
}

window.onload = () => {
    window.removeLoader();

    if (process.env.PRODUCTION) {
        console.log = () => {};
    }

    init();
};

function init(): void {
    new Game();
}